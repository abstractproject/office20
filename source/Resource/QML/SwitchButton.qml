import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Rectangle {

    color: "#2b579a"
    visible: true;

    Material.theme: Material.Dark
    Material.accent: Material.Blue

    RowLayout {
        anchors.fill: parent


        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        CheckBox {
            objectName: "Switch";
            text: "Don't talk to Spectrum anymore"
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }

    signal buttonClicked_enable()
}
