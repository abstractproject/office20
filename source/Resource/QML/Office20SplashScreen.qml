import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Material 2.12


Rectangle {
    visible: true
    color: "#2b579a"

    Material.accent: "white"
    Material.theme: Material.Dark

    ColumnLayout {
        anchors.centerIn: parent
        spacing: 6

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Rectangle {
            height: 127
            Layout.fillWidth: true
            color: "transparent"

            Image {
                anchors.centerIn: parent
                source: "../AppIcons/blackandwhite/WinWordLogoSmall.contrast-black_scale-180.png"
            }
        }

        Label {
            Layout.fillWidth: true
            text: "Office20"
            color: "white"
            font.pointSize: 24
            font.family: "Open Sans Light"
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            Layout.fillWidth: true
            color: "white"
            
            text: qsTr("Getting Word ready for you...")
            horizontalAlignment: Text.AlignHCenter
            font.family: "Open Sans"
        }

        Item { height: 30 }

        Rectangle {
            Layout.fillWidth: true
            BusyIndicator {
                anchors.centerIn: parent
                running: true
                height: 48
                width: 48
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}

