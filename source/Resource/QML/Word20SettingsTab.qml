import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ScrollView {
    Material.accent: Material.Blue
    Material.theme: Material.Light
    visible: true


    background: Rectangle {
        color: "#f0f0f0"
    }

    clip: true

    ColumnLayout {
        spacing: 6

        width: 250

        Label {
            text: "General Options"
            font.pointSize: 11
        }

        Switch {
            objectName: "displayAnim"
            text: "Display animations"
            font.pointSize: 10
        }

        /*Switch {
            id: useUniversalStyle
            text: "Use Windows Universal style"
        }*/

        Switch {
            objectName: "talkToSpectrum"
            text: "Talk to Spectrum"
            font.pointSize: 10
        }

        Label {
            text: "User Information and Personalization"
            font.pointSize: 11
        }

        TextField {
            objectName: "userName"
            Layout.fillWidth: true
            placeholderText: "My name is Spectrum. Tell me yours."
            font.pointSize: 10
        }

        TextField {
            objectName: "userInitials"
            Layout.fillWidth: true
            placeholderText: "How should I abbreviate your name?"
            font.pointSize: 10
        }

        Label {
            text: "Startup Options"
            font.pointSize: 11
            font.family: "Open Sans"
        }

        Switch {
            objectName: "recoverWindowState"
            text: "Recover window state"
            font.pointSize: 10
        }

        Switch {
            objectName: "showStartScreen"
            text: "Show the start screen at startup"
            font.pointSize: 10
        }

        Switch {
            objectName: "displaySplashScreen"
            text: "Display splash screen"
            font.pointSize: 10
        }

/*
        Label {
            color: "#2b579a"
            font.pointSize: 11
            text: "Spectrum Privacy Notice"
        }

        Label {
            color: "#2b579a"
            text: "Don't worry about your privacy: I don't send any of your data to external sources."
        }*/

        signal buttonClicked_enable()
    }
}
