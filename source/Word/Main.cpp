/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <QApplication>
#include <QCommandLineParser>
#include <QFile>
#include <QStyleFactory>
#include <QQuickStyle>

#include "WordInterface.h"

int CreateBreezePalette() {
    QPalette wordColors;

    wordColors.setColor(QPalette::Window, QColor("#eff0f1"));
    wordColors.setColor(QPalette::WindowText, QColor("#414445"));
    wordColors.setColor(QPalette::Base, QColor("#fcfcfc"));
    wordColors.setColor(QPalette::AlternateBase, QColor("#f0f0f0"));
    wordColors.setColor(QPalette::ToolTipBase, QColor("#f0f0f0"));
    wordColors.setColor(QPalette::ToolTipText, Qt::black);
    wordColors.setColor(QPalette::Text, QColor("#414445"));
    wordColors.setColor(QPalette::Button, QColor("#eeeeee"));
    wordColors.setColor(QPalette::ButtonText, QColor("#26292a"));
    wordColors.setColor(QPalette::BrightText, Qt::red);
    wordColors.setColor(QPalette::Link, QColor("#9cc1da"));
    wordColors.setColor(QPalette::Highlight, QColor("#3daee9"));
    wordColors.setColor(QPalette::HighlightedText, Qt::white);

    qApp->setPalette(wordColors);
    qApp->setStyleSheet("QToolTip { color: white; background-color: #232627; border: 1px solid #585b5b; }");

    QQuickStyle::setStyle("Material");
}

bool isColorDark(QColor color){

    double darkness = 1-(0.299*color.red() + 0.587*color.green() + 0.114*color.blue())/255;

    if(darkness<0.5){
        return false; // It's a light color
    }else{
        return true; // It's a dark color
    }
}

int main(int argc, char *argv[])
{
    #ifdef Q_OS_LINUX
    qputenv("QT_STYLE_OVERRIDE","Breeze");
    QIcon::setThemeName("breeze");
    #endif

    QApplication a(argc, argv);

    a.setStyle(QStyleFactory::create("Breeze"));
    CreateBreezePalette();

    QFont f("Open Sans", 10);
    a.setFont(f);

    //if (isColorDark(QPalette().brush((QPalette::Window)).color())) QIcon::setThemeName("Papirus-Dark");

    /*#ifdef Q_OS_DARWIN
    QFile File(":/light.qss");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    qApp->setStyleSheet(StyleSheet);
    #endif*/

    QCoreApplication::setOrganizationName("Abstract Software Project");
    QCoreApplication::setApplicationName("org.office20.word");
    QCoreApplication::setApplicationVersion("20.0.13");

    QCommandLineParser parser;
    parser.addPositionalArgument("file", QCoreApplication::translate("main", "File to open."));
    parser.process(a);
    const QStringList args = parser.positionalArguments();

    WordInterface msword;
    if (args.size() > 0)
        msword.OpenFile(args.at(0));
    msword.Begin();

    return a.exec();
}
