#include "WordInterface.h"
#include "src/ui_BaseUserInterface.h"

#include <QMessageBox>

bool WordInterface::eventFilter(QObject *object, QEvent *event) {
    if (object == ui->docView && event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        checkReturnKey(keyEvent);
    }
    
    return QMainWindow::eventFilter(object, event);
}

void WordInterface::closeEvent(QCloseEvent* event)
{
    if (nostart && document.filename == "") {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Spectrum has a question");
        msgBox.setText("Sure you want to exit?");
        msgBox.setInformativeText("This document hasn't been saved yet, so I couldn't AutoSave it.");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        msgBox.setIcon(QMessageBox::Question);
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Save:
            ui->save->click();
            break;
        case QMessageBox::Cancel:
            event->ignore();
            break;
        }
    }

    WriteOptions();
}

/*Display window title on interface */
void WordInterface::setWindowTitle(QString name)
{
    if (!nostart && name == "" && document.filename == "") {
        name = "Word";
	goto Autosave;
    }

    if (name == "") {
        if (document.filename == "") name = defaultname;
	else name = QFileInfo(document.filename).fileName();

        if (document.filename != "" && ui->enableautosave->isChecked()) {
            name += " - (Autosave)";
	    goto Autosave;
	}

        if (!ui->docView->document()->isModified()) {
           if (document.filename == "") name += " - Word";
	   else
#ifdef Q_OS_DARWIN
           name += " - Saved to your Mac";
#else
           name += " - Saved to your Linux PC";
#endif
	} else {
            name += " - Modified";
	}
    }

    Autosave:

    QMainWindow::setWindowTitle(name);
}
