#include "WordInterface.h"
#include "src/ui_BaseUserInterface.h"

#include <QQuickItem>

void WordInterface::WriteOptions()
{
    bool displayAnim = ui->settingsview->rootObject()->findChild<QObject*>("displayAnim")->property("checked").toBool();
    bool talkToSpectrum = ui->settingsview->rootObject()->findChild<QObject*>("talkToSpectrum")->property("checked").toBool();
    bool talkToSpectrum_2 = ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("Switch")->property("checked").toBool();
    QString userName = ui->settingsview->rootObject()->findChild<QObject*>("userName")->property("text").toString();
    QString userInitials = ui->settingsview->rootObject()->findChild<QObject*>("userInitials")->property("text").toString();
    bool recoverWindowState = ui->settingsview->rootObject()->findChild<QObject*>("recoverWindowState")->property("checked").toBool();
    bool showSS = ui->settingsview->rootObject()->findChild<QObject*>("showStartScreen")->property("checked").toBool();
    bool displaySS = ui->settingsview->rootObject()->findChild<QObject*>("displaySplashScreen")->property("checked").toBool();

    QSettings settings;
    settings.beginGroup("UserSettings");
    settings.setValue("UserName", userName);
    settings.setValue("UserInitials", userInitials);
    settings.setValue("ShowStartScreen", recoverWindowState);
    settings.setValue("UseAnimations", displayAnim);
    settings.setValue("RecoverState", recoverWindowState);
    settings.setValue("SplashScreen", displaySS);
    //settings.setValue("Autosave", ui->enableautosave->isChecked());
    settings.setValue("ShowSpectrumSidebar", ui->sidebarframe->isHidden());

    if (settings.value("TalkToSpectrum", false).toBool()) {
        settings.setValue("TalkToSpectrum", !talkToSpectrum_2);
        //if (!ui->name->isHidden()) settings.setValue("UserName", ui->name->text());
    } else
        settings.setValue("TalkToSpectrum", talkToSpectrum);

    //settings.setValue("SidebarIndex", ui->sidebartabs->currentIndex());
    if (!plaintextmode) {
        settings.setValue("ShowRibbon", ui->ribbon->isHidden());
        settings.setValue("ShowTabs", tabshidden);
    }

    settings.setValue("ShowAutoSaveAlert", ui->autosavealert->isHidden());
    settings.setValue("ShowTabs", tabshidden);
    settings.setValue("SpectrumHistory", searchlist);
    //settings.setValue("BlueStartScreen", ui->showbluescreen->isChecked());
   // settings.setValue("FirstTime", false);
    settings.endGroup();

    settings.beginGroup("WindowSettings");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}

void WordInterface::ReadOptions()
{
    QSettings settings;
    settings.beginGroup("UserSettings");
    QString userName = settings.value("UserName", "").toString();
    QString userInitials = settings.value("UserInitials", "").toString();
    bool showStartScreen = settings.value("ShowStartScreen", true).toBool();
    bool displayAnim = settings.value("UseAnimations", true).toBool();
    bool recoverState = settings.value("RecoverState", true).toBool();
    bool splashScreen = settings.value("SplashScreen", true).toBool();
    //ui->enableautosave->setChecked(settings.value("Autosave", true).toBool());
    ui->sidebarframe->setHidden(settings.value("ShowSpectrumSidebar", false).toBool());
    //ui->sidebartabs->setCurrentIndex(settings.value("SidebarIndex", 1).toInt());
    bool hideribbon = settings.value("ShowRibbon", false).toBool();
    ui->autosavealert->setHidden(settings.value("ShowAutoSaveAlert", false).toBool());
    tabshidden = settings.value("ShowTabs", false).toBool();
    bool firsttime = settings.value("FirstTime", true).toBool();
    searchlist = settings.value("SpectrumHistory", searchlist).value<QStringList>();
    //ui->showbluescreen->setChecked(settings.value("BlueStartScreen", false).toBool());

    bool talkToSpectrum = settings.value("TalkToSpectrum", false).toBool();
    settings.endGroup();

    spectrumOnly = talkToSpectrum;

    ui->settingsview->rootObject()->findChild<QObject*>("displayAnim")->setProperty("checked", displayAnim);
    ui->settingsview->rootObject()->findChild<QObject*>("talkToSpectrum")->setProperty("checked", talkToSpectrum);
    ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("Switch")->setProperty("checked", !talkToSpectrum);
    ui->settingsview->rootObject()->findChild<QObject*>("showStartScreen")->setProperty("checked", showStartScreen);
    ui->settingsview->rootObject()->findChild<QObject*>("recoverWindowState")->setProperty("checked", recoverState);
    ui->settingsview->rootObject()->findChild<QObject*>("displaySplashScreen")->setProperty("checked", splashScreen);

    ui->settingsview->rootObject()->findChild<QObject*>("userName")->setProperty("text", userName);
    ui->settingsview->rootObject()->findChild<QObject*>("userInitials")->setProperty("text", userInitials);


    ui->helpindex->setCurrentIndex(0);

    if (talkToSpectrum) defaultHome = 3;

/*    if (!ui->showbluescreen->isChecked()) {
        //ui->home->setStyleSheet(ui->home->styleSheet()+"#home{background-color: transparent;}QWidget{color: black}QToolButton:hover{background-color:#d5e1f2;}QToolButton:pressed {background-color: #c0d3f0;}");
        //ui->blankfromtemplate->setIcon(QIcon(":/basic/document-text-picture_large@1x.png"));
        //ui->openstart->setIcon(QIcon(":/basic/computer-browse_large@1x.png"));
        //ui->startsave->setIcon(QIcon(":/basic/save-filled_large@1x.png"));
    }*/

    if (!hideribbon) {
        if (tabshidden) HideTabs();
        else ShowTabs();
    } else HideRibbon();

    //ui->optionsUserName->setText("Miles Avery");
    //ui->greeting->setText("Good Morning.");
    //ui->user->setText(ui->optionsUserName->text());
    ui->screenswitch->setAnim(displayAnim);

    if (ui->recoversize->isChecked()) {
        settings.beginGroup("WindowSettings");
        resize(settings.value("size", QSize(1,1)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
        settings.endGroup();
    }
}
