/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "WordInterface.h"
#include "src/ui_BaseUserInterface.h"
#include <converter.h>

#include <QtWidgets>

void WordInterface::checkReturnKey(QKeyEvent *keyEvent) {
    if (keyEvent->key() == Qt::Key_Return)
        if (ui->docView->textCursor().blockFormat().headingLevel() != 0)
            ui->eraseFormat->click();
}

void WordInterface::GenerateAutoStyles(OOO::StyleInformation* mStyleInformation)
{
    stylenames << "Normal" << "Heading1" << "Heading2" << "Heading3" << "Heading4" << "Heading5" <<
                  "Heading6" << "Heading7" << "Heading8" << "Heading9" << "Title" << "Subtitle" <<
                  "Emphasis" <<  "Strong" << "Quote" << "Reference" << "BookTitle";
    /*
     Normal: 11pt
     No Spacing: line spacing: 100%
     Heading 1: color: #2f5496, 16pt, Light
     Heading 2: #5367a1, 13pt, Light
     Heading 3: #647492, 12pt, Light
     Heading 4: #758eb7, Italic, Light, 11pt
     Heading 5: #5976a9, Light, 11pt
     Heading 6: #8c98ae, Light, 11pt
     Heading 7: #808da5, Italic, 11pt
     Heading 8: 10.5pt, Light
     Heading 9: 10.5pt, Italic, Light
     Title: 28pt, Light
     Subtitle: #7c7d7d, 11pt (Italic?)
    Subtle Emphasis: Italic
    Emphasis: Italic
    Intense Emphasis: #547cc5, Italic
    Strong: Bold

    Quote: Italic, Center, Before=10pt After=8pt, Spacing=108%
    Intense Quote: (Same as Quote) + #567cc5

    SUBTLE REFERENCE: All Caps, #5a5a5a
    INTENSE REFERENCE: All Caps, #4472c4
    Book Title: Bold, Italic
    List Paragraph: (list with no bullets)
    */

    stylemenus = new QMenu(this);

    for (QString stylename : stylenames) {
        QTextCharFormat fmt;
        QTextBlockFormat bfmt = ui->docView->textCursor().blockFormat();

        /*TODO bfmt.setHeadingLevel(...); then have a sidebar with heading navigation */

        if (stylename == "Normal") ; // do nothing
        else if (stylename == "Heading 1") {
            fmt.setForeground(QBrush("#2f5496"));
            fmt.setFontPointSize(16);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(1);
        } else if (stylename == "Heading 2") {
            fmt.setForeground(QBrush("#5367a1"));
            fmt.setFontPointSize(13);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(2);
        } else if (stylename == "Heading 3") {
            fmt.setForeground(QBrush("#647492"));
            fmt.setFontPointSize(12);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(3);
        } else if (stylename == "Heading 4") {
            fmt.setForeground(QBrush("#758eb7"));
            fmt.setFontPointSize(11);
            fmt.setFontItalic(true);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(4);
        } else if (stylename == "Heading 5") {
            fmt.setForeground(QBrush("#5976a9"));
            fmt.setFontPointSize(11);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(5);
        } else if (stylename == "Heading 6") {
            fmt.setForeground(QBrush("#8c98ae"));
            fmt.setFontPointSize(11);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(6);
        } else if (stylename == "Heading 7") {
            fmt.setForeground(QBrush("#808da5"));
            fmt.setFontPointSize(11);
            fmt.setFontItalic(true);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(7);
        } else if (stylename == "Heading 8") {
            fmt.setForeground(QBrush("#2b579a"));
            fmt.setFontPointSize(10.5);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(8);
        } else if (stylename == "Heading 9") {
            fmt.setForeground(QBrush("#2b579a"));
            fmt.setFontPointSize(10.5);
            fmt.setFontItalic(true);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(9);
        } else if (stylename == "Title") {
            fmt.setFontPointSize(28);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(1);
        } else if (stylename == "Subtitle") {
            fmt.setForeground(QBrush("#7c7d7d"));
            fmt.setFontPointSize(11);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(1);
        } else if (stylename == "Emphasis") {
            fmt.setFontItalic(true);
        } else if (stylename == "Strong") {
            fmt.setFontWeight(QFont::Bold);
        } else if (stylename == "Quote") {
            fmt.setFontItalic(true);
            bfmt.setAlignment(Qt::AlignHCenter);
            bfmt.setTopMargin(10);
            bfmt.setBottomMargin(8);
            bfmt.setLineHeight(108, QTextBlockFormat::ProportionalHeight);
        } else if (stylename == "Reference") {
            fmt.setFontCapitalization(QFont::AllUppercase);
            fmt.setForeground(QBrush("#5a5a5a"));
        } else if (stylename == "Book Title") {
            fmt.setFontItalic(true);
            fmt.setFontWeight(QFont::Bold);
        }

        if (mStyleInformation != nullptr) {
             QString ooo = stylename.remove(" ");
             if (mStyleInformation->containsStyleFormatProperty(ooo)) {
                    auto textFormat = mStyleInformation->styleProperty(ooo).textFormat();
		    //qDebug() << textFormat.mFontFamily;
		    textFormat.apply(&fmt);
		    auto paraFormat = mStyleInformation->styleProperty(ooo).paraFormat();
                    paraFormat.apply(&bfmt);
                    //if (textFormat.mHasFontFamily)
		    //qDebug() << textFormat.mFontFamily;
		    //qDebug() << textFormat.mFontFamily;
	    }
	}

        styleCharFormats[stylename] = fmt;
        styleBlockFormats[stylename] = bfmt;

	QAction* styleaction = stylemenus->addAction(stylename);
	//styleaction->setFont(fmt.font());
        
	connect(styleaction, SIGNAL(triggered()), SLOT(ConnectStyleSlots()));
    }

    // ONLY SET MENU IF NULL
    if (ui->stylebutton->menu() == nullptr) ui->stylebutton->setMenu(stylemenus);
}

void WordInterface::ConnectStyleSlots()
{
   QString stylename = sender()->property("text").toString();

   /* Easy to apply styles
      XXX something better than merge?
    */

   //qDebug() << stylename << "\n" << styleCharFormats[stylename];

   QTextCursor cc = ui->docView->textCursor();

   bool hadSelection = cc.hasSelection();

   if (!hadSelection)
       cc.select(QTextCursor::BlockUnderCursor);

   if (!cc.hasSelection()) cc.clearSelection();

   cc.setBlockFormat(styleBlockFormats[stylename]);
   cc.setCharFormat(styleCharFormats[stylename]);

   if (!hadSelection)
       cc.clearSelection();

   ui->docView->setTextCursor(cc);

   onCursorUpdate(); // update buttons
}
