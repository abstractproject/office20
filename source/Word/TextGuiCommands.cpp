#include "WordInterface.h"
#include "src/ui_BaseUserInterface.h"

#include <converter.h>

#include <QTest>
#include <QQuickItem>
#include <QtWidgets>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

void WordInterface::setTextSpacing() {
    qreal height = 1;
    if (sender() == spacingList[0]) height = 100;
    else if (sender() == spacingList[1]) height = 115;
    else if (sender() == spacingList[2]) height = 150;
    else if (sender() == spacingList[3]) height = 200;
    else if (sender() == spacingList[4]) height = 250;

    QTextCursor cursor = ui->docView->textCursor();
    QTextBlockFormat fmt = cursor.blockFormat();
    fmt.setLineHeight(height, QTextBlockFormat::ProportionalHeight);
    cursor.setBlockFormat(fmt);
    ui->docView->setTextCursor(cursor);
}

void WordInterface::setTextListOn() {
    QTextListFormat::Style style = QTextListFormat::ListDecimal; // default
    if (sender() == bulletList[0]) style = QTextListFormat::ListDisc;
    else if (sender() == bulletList[1]) style = QTextListFormat::ListCircle;
    else if (sender() == bulletList[2]) style = QTextListFormat::ListSquare;
    else if (sender() == bulletList[3]) style = QTextListFormat::ListDecimal;
    else if (sender() == bulletList[4]) style = QTextListFormat::ListLowerAlpha;
    else if (sender() == bulletList[5]) style = QTextListFormat::ListUpperAlpha;
    else if (sender() == bulletList[6]) style = QTextListFormat::ListLowerRoman;
    else if (sender() == bulletList[7]) style = QTextListFormat::ListUpperRoman;

    if (ui->bulletedlist->isChecked() or sender() != ui->bulletedlist) {
        QTextCursor cursor = ui->docView->textCursor();
        QTextListFormat listFormat;
        listFormat.setStyle( style );
        cursor.createList( listFormat );
        ui->bulletedlist->setChecked(true);
    } else {
        RemoveList();
    }
}

void WordInterface::RemoveList()
{
    QTextCursor cursor = ui->docView->textCursor();
    QTextList *list = cursor.currentList();
    if( list ) {
        QTextListFormat listFormat;
        listFormat.setIndent( 0 );
        //listFormat.setStyle( QTextList );
        list->setFormat( listFormat );
        for( int i = list->count() - 1; i >= 0 ; --i )
            list->removeItem( i );
     }
}

void WordInterface::BeginConnections()
{
    connect(ui->ribbon, SIGNAL(currentChanged(int)), SLOT(SwitchScreens())); // tabBarDoubleClicked(int)
    connect(ui->goback, SIGNAL(released()), SLOT(SwitchScreens()));
    connect(ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("backtoedit"), SIGNAL(released()), SLOT(SwitchScreens()));

    connect(ui->shrinkSidebar, &QToolButton::released, [=]() {
        if (isSidebarMinimized) {
            QPropertyAnimation *animation = new QPropertyAnimation(ui->goback, "minimumSize");
            animation->setDuration(100);
            animation->setStartValue(QSize(51, 0));
            animation->setEndValue(QSize(100, 0));

            animation->start();

            connect(animation, &QPropertyAnimation::finished, [=](){ 
                ui->goback->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->Home->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->New->setStyleSheet("margin-bottom: 0px;  min-width: 100px; padding-left: 10px;");
                ui->Open->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->Save->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->Options->setStyleSheet("padding-left: 10px; min-width: 100px;");

                ui->shrinkSidebar->setArrowType(Qt::LeftArrow);
                ui->goback->setToolButtonStyle(Qt::ToolButtonTextBesideIcon); 
                ui->Home->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->New->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->Open->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->Save->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->Options->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
            });
        } else {
            ui->goback->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Home->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->New->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Open->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Save->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Options->setToolButtonStyle(Qt::ToolButtonIconOnly);


            QPropertyAnimation *animation = new QPropertyAnimation(ui->goback, "minimumSize");
            animation->setDuration(100);
            animation->setStartValue(QSize(100, 0));
            animation->setEndValue(QSize(51, 0));

            ui->goback->setStyleSheet("margin-bottom: 0px;");
            ui->Home->setStyleSheet("margin-bottom: 0px;");
            ui->New->setStyleSheet("margin-bottom: 0px;;");
            ui->Open->setStyleSheet("margin-bottom: 0px;");
            ui->Save->setStyleSheet("margin-bottom: 0px;");
            ui->Options->setStyleSheet("");


            animation->start();

            connect(animation, &QPropertyAnimation::finished, [=](){
                ui->shrinkSidebar->setArrowType(Qt::RightArrow);
            });
        }

        isSidebarMinimized = !isSidebarMinimized;
    });

    connect(ui->blankfromtemplate, SIGNAL(released()), SLOT(CreateBlankDocument()));
    connect(ui->newdoc2, SIGNAL(released()), SLOT(CreateBlankDocument()));
    connect(ui->createsimple, SIGNAL(released()), SLOT(CreateBlankDocument()));

    connect(ui->openfilelocation, SIGNAL(released()), SLOT(OpenFileLocation()));

    connect(ui->indentmore, &QToolButton::released, [=](){
    QTextCursor cursor = ui->docView->textCursor();
    QTextBlockFormat fmt = cursor.blockFormat();
    fmt.setTextIndent(fmt.textIndent()+1);
    cursor.setBlockFormat(fmt);
    ui->docView->setTextCursor(cursor);
    });

    connect(ui->indentless, &QToolButton::released, [=](){
    QTextCursor cursor = ui->docView->textCursor();
    QTextBlockFormat fmt = cursor.blockFormat();
    fmt.setTextIndent(fmt.textIndent()-1);
    cursor.setBlockFormat(fmt);
    ui->docView->setTextCursor(cursor);
    });

    connect(ui->zoomIn, &QToolButton::released, [=]() { ui->readingBrowser->zoomIn(2); });
    connect(ui->zoomOut, &QToolButton::released, [=]() { ui->readingBrowser->zoomOut(2); });

    connect(ui->closesidebar, SIGNAL(released()), SLOT(CloseSidebar()));
    connect(ui->spectrumintro, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->whatsnew, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->helphome, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->ribbonhelp, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribboninfo, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribbonlicense, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribbonstar, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribbonspectrum, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->aboutword, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->office20license, SIGNAL(released()), SLOT(HelpIndex()));

   connect(ui->insertImage, &QToolButton::released, [=]() {
           QFileDialog qf;
           qf.selectFile(defaultname);
           qf.setAcceptMode(QFileDialog::AcceptOpen);
           qf.setNameFilter("Support Image Formats (*.bmp *.jpg *.jpeg *.gif *.png *.svg);; All Files (*)");
           if (qf.exec() == QDialog::Rejected) return;
           QString file = qf.selectedFiles()[0];
           QUrl Uri (QString ( "file://%1" ).arg ( file ));

           QImage image = QImageReader ( file ).read();
           if (image.width() > 600) image = image.scaled(600, 600, Qt::KeepAspectRatio);

           QTextDocument * textDocument = ui->docView->document();
           textDocument->addResource(QTextDocument::ImageResource, Uri, QVariant ( image ));

           QTextCursor cursor = ui->docView->textCursor();
           QTextImageFormat imageFormat;
           imageFormat.setWidth( image.width() );
           imageFormat.setHeight( image.height() );
           imageFormat.setName( Uri.toString() );
           cursor.insertImage(imageFormat);
           ui->docView->setTextCursor(cursor);
           });

   connect(ui->setOverline, &QToolButton::released, [=]() {
           QTextCursor cursor = ui->docView->textCursor();
           QTextCharFormat fmt = cursor.blockCharFormat();
           fmt.setFontOverline(ui->setOverline->isChecked());
           cursor.setCharFormat(fmt);
           ui->docView->setTextCursor(cursor); 
           });

   connect(ui->insertTable, &QToolButton::released, [=]() {
           QTextTableFormat fmt;
           fmt.setCellSpacing(0);
           fmt.setCellPadding(5);
           fmt.setWidth(QTextLength(QTextLength::PercentageLength, 150));
           fmt.setBorder(0.4);
           //fmt.setBorderBrush(QBrush("#000000"));

           QVector<QTextLength> constr(tablew->value()*tableh->value());
           constr.fill(QTextLength(QTextLength::PercentageLength, 150/tablew->value()));

           fmt.setColumnWidthConstraints(constr);
           QTextCursor cursor = ui->docView->textCursor();
           QTextTable *table = cursor.insertTable(tablew->value(), tableh->value(), fmt);
           });

   connect(ui->insertLink, &QToolButton::released, [=]() {
           QTextCursor cursor = ui->docView->textCursor(); cursor.insertHtml("<a href='" + url->text() + "'>" + url->text() + "</a>");
           });
   connect(url, &QLineEdit::returnPressed, [=]() {
           QTextCursor cursor = ui->docView->textCursor(); cursor.insertHtml("<a href='" + url->text() + "'>" + url->text() + "</a>");
           });

    connect(ui->checkspell, &QToolButton::released, ui->docView, &KTextEdit::checkSpelling);
    connect(ui->doreplace, &QToolButton::released, ui->docView, &KTextEdit::replace);

    ui->insertPageBreak->hide(); // XXX add page break thingy
    connect(ui->setStrikeout,   &QToolButton::released, [=]() { ui->docView->setTextStrikeOut(ui->setStrikeout->isChecked()); });
    connect(ui->setSuperscript, &QToolButton::released, [=]() { ui->docView->setTextSuperScript(ui->setSuperscript->isChecked()); });
    connect(ui->setSubscript,   &QToolButton::released, [=]() { ui->docView->setTextSubScript(ui->setSubscript->isChecked()); });
    connect(ui->setBold,   &QToolButton::released, [=]() { ui->docView->setTextBold(ui->setBold->isChecked()); });
    connect(ui->setItalic, &QToolButton::released, [=]() { ui->docView->setTextItalic(ui->setItalic->isChecked()); });
    connect(ui->setUnderline, &QToolButton::released, [=]() { ui->docView->setTextUnderline(ui->setUnderline->isChecked()); });

    connect(ui->setColor,   SIGNAL(released()), SLOT(EditingCommands()));
    connect(ui->setBgColor, SIGNAL(released()), SLOT(EditingCommands()));

    connect(ui->fontSizeComboBox,  SIGNAL(currentTextChanged(QString)), SLOT(EditingCommands()));
    connect(ui->fontComboBox,      SIGNAL(currentTextChanged(QString)), SLOT(EditingCommands()));
    connect(ui->eraseFormat,       SIGNAL(released()), SLOT(EditingCommands()));
    connect(ui->alignleft,   SIGNAL(released()), ui->docView, SLOT(alignLeft()));
    connect(ui->alignright,  SIGNAL(released()), ui->docView, SLOT(alignRight()));
    connect(ui->aligncenter, SIGNAL(released()), ui->docView, SLOT(alignCenter()));
    connect(ui->alignjustify,SIGNAL(released()), ui->docView, SLOT(alignJustify()));

    //connect(ui->upload,     SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->openButton, SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->openMSWordButton, SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->openTextFileButton, SIGNAL(released()), SLOT(IoCommands()));
    //connect(ui->browseDownloads, SIGNAL(released()), SLOT(IoCommands()));
    //connect(ui->browseDocuments, SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->openstart,  SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->save,      SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->saveAs,    SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->saveAsPDF, SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->saveAsWeb, SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->deleteFile, SIGNAL(released()), SLOT(IoCommands()));
    connect(ui->print, SIGNAL(released()), SLOT(IoCommands()));

    connect(ui->docView, SIGNAL(textChanged()), SLOT(slotSetTitle()));
    connect(ui->docView, SIGNAL(textChanged()), SLOT(onTextChanged()));
    connect(ui->docView, SIGNAL(cursorPositionChanged()), SLOT(onCursorUpdate()));
    connect(ui->docView->document(), SIGNAL(modificationChanged(bool)), SLOT(slotSetTitle()));

    connect(ui->Home, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->New,  SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->Open, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->Options, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->Save, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));

    connect(ui->startHome, SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->startNew,  SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->startOpen, SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->startOptions, SIGNAL(released()), SLOT(SidebarSwitcher()));

    ui->scrollview->hide();
    ui->showInFullscreen->hide();

    connect(ui->pageview, SIGNAL(released()), SLOT(SetView()));
    connect(ui->webview,  SIGNAL(released()), SLOT(SetView()));
    connect(ui->scrollview,  SIGNAL(released()), SLOT(SetView()));
    connect(ui->readingMode, SIGNAL(released()), SLOT(SetView()));
    connect(ui->closeReadingMode, &QToolButton::released, [=](){
            ui->viewModes->slideInIdx(0);

	    if (isFullScreen()) {
	        showNormal();
	        if (wasMaximized)
                      setWindowState(windowState() ^ Qt::WindowMaximized), qDebug("a");
	        else
                    showNormal(), qDebug("b");
            }

            if (ui->docView->usePageMode()) {
                if (ui->docView->isPageContinuous()) ui->scrollview->setChecked(true);
                else ui->pageview->setChecked(true);
            } else {
                ui->webview->setChecked(true);
            }
    });

    connect(ui->readAloud, &QToolButton::released, [=]() {
		        if (ui->readAloud->isChecked())
		            reader.stop();
		        else {
		            reader.say(ui->docView->textCursor().selectedText());
		        }
		    });

    /*connect(ui->showInFullscreen, &QToolButton::released, [=]() {
        if (ui->showInFullscreen->isChecked()) showFullScreen();
	else showNormal();
        wasMaximized = isMaximized();
   });*/

    //connect(ui->audioMode,   SIGNAL(released()), SLOT(SetView()));


    //connect(ui->enableSpectrum, &QToolButton::released, [=]() { ui->spectrumSearch->setHidden(!ui->enableSpectrum->isChecked()); });
}

void WordInterface::IoCommands()
{
    if (nostart) {
        ShowScreen("EDIT");
        ui->ribbon->setCurrentIndex(1);
    }

    if (sender() == ui->openButton or sender() == ui->openstart or sender() == ui->openMSWordButton or sender() == ui->openTextFileButton) {
        QFileDialog qf;
	qf.selectFile(defaultname);
	qf.setAcceptMode(QFileDialog::AcceptOpen);
/*        if (sender() == ui->browseDownloads) qf.setDirectory(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        else if (sender() == ui->browseDocuments) qf.setDirectory(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
        else qf.setDirectory(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));*/

	qf.setNameFilter("Supported Formats (*.html *.xhtml *.odt *.ott);;"
			 "HTML Files (*.html *.xhtml);;"
                         "Open Document Text (*.odt *.ott);;"
			 "Microsoft OOXML (*.docx *.dotx);;");

       if (sender() == ui->openMSWordButton) qf.setNameFilter("Microsoft Office Open XML (*.docx *.dotx)");
       else if (sender() == ui->openTextFileButton) qf.setNameFilter("Plain Text Files (*)");

	if (qf.exec() == QDialog::Rejected) return;
	if (!nostart or (ui->docView->toPlainText() == "" && document.filename == "")) OpenFile(qf.selectedFiles()[0], ui->openLocked->isChecked(), ui->openLocked->isChecked());
	else {
            WordInterface* w = new WordInterface();
	    w->OpenFile(qf.selectedFiles()[0]);
            w->Begin();
	}

        ui->docView->document()->setModified(false);
	nostart = true;
    } else if (sender() == ui->save or sender() == ui->saveAs or sender() == ui->saveAsWeb or sender() == ui->saveAsPDF) {
        QString filename, suffix;
	bool temp = false;
	if (sender() == ui->saveAs or sender() == ui->saveAsWeb or sender() == ui->saveAsPDF) temp = true;

        if (document.filename == "" or temp) {
            QFileDialog qf;
            qf.selectFile(defaultname);
            qf.setAcceptMode(QFileDialog::AcceptSave);
            qf.setNameFilter("Supported Formats (*.html *.htm *.odt *.ott);; Open Document Text (*.odt *.ott);;"
                             "HTML Files (*.xhtml *.html *.htm);; All Files (*)");

	    if (sender() == ui->saveAsWeb) qf.setNameFilter("Web Page (*.html)");
	    if (sender() == ui->saveAsPDF) qf.setNameFilter("PDF Files (*.pdf)");
            if (qf.exec() == QDialog::Rejected)
                return; //false

            filename = qf.selectedFiles()[0];
            suffix = QFileInfo(filename).suffix();
        }

        bool sucess;
        QTextDocumentWriter writer(filename);
        if (suffix == "odt" or suffix == "ott") {
            writer.setFormat("ODT");
        } else if (suffix == "html" or suffix == "xhtml") {
	    writer.setFormat("HTML");
	} else if (suffix == "pdf") {
            QPrinter printer(QPrinter::PrinterResolution);
            printer.setOutputFormat(QPrinter::PdfFormat);
            printer.setOutputFileName(filename);
            printer.setPageSize(QPrinter::Letter);
            printer.setPageMargins(.1, .1, .1, .1, QPrinter::Inch);
            QSizeF pageSizeBefore = ui->docView->document()->pageSize();
            ui->docView->document()->setPageSize(printer.pageRect().size());
            ui->docView->document()->print(&printer);
            ui->docView->document()->setPageSize(pageSizeBefore);
	    goto nowrite;
	} else {
	    writer.setFormat("PLAINTEXT");
        }

        sucess = writer.write(ui->docView->document());
	if (!temp && document.filename == "") {
            document.filename = filename;
	    document.suffix = suffix;
	}

        ui->docView->document()->setModified(false);
        setWindowTitle();
        nowrite: ;
    } else if (sender() == ui->deleteFile) {
        QFile file(document.filename, this);

	QMessageBox msgBox;
       // msgBox.setTitle("Spectrum has a question");
        msgBox.setText("Sure you want to delete " + QFileInfo(document.filename).fileName() + "?");
	msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setIcon(QMessageBox::Question);
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Yes:
	    file.remove();
	    close();
        break;
        }
    } else if (sender() == ui->print) {
        QPrinter printer(QPrinter::PrinterResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        QPrintDialog printDialog(&printer);
        if (printDialog.exec() == QDialog::Accepted) {
            printer.setPageSize(QPrinter::A3);
            printer.setPageMargins(1, 1, 1, 1, QPrinter::Inch);
            QSizeF pageSizeBefore = ui->docView->document()->pageSize();
            ui->docView->document()->setPageSize(printer.pageRect().size());
            ui->docView->document()->print(&printer);
            ui->docView->document()->setPageSize(pageSizeBefore);
        }
    }

    if (nostart) {
        ShowScreen("EDIT");
        ui->ribbon->setCurrentIndex(1);
    }

    if (document.filename != "") {
        ui->startsave->hide();
        ui->save->hide();
        ui->autosavealert_2->show();
        ui->openfilelocation->show();
    }

    //setWindowTitle();
}

void WordInterface::SaveDocument()
{
    if (!locksave) locksave = true;
    else return;

/*    if (document.suffix == "docx" or document.suffix == "dotx") {
        QProcess docx;
        docx.start("pandoc -f html -t docx -o " + document.filename);
        docx.waitForStarted();
        docx.write(ui->docView->toHtml().toUtf8());
        docx.closeWriteChannel();
        docx.waitForFinished();
    } else*/ 

     QTextDocumentWriter writer(document.filename);
     if (document.suffix == "odt" or document.suffix == "ott")
         writer.setFormat("ODT");
     else if (document.suffix == "html" or document.suffix == "xhtml")
        writer.setFormat("HTML");
     else
        writer.setFormat("plaintext");

    writer.write(ui->docView->document());
    ui->docView->document()->setModified(false);
    nostart = true;

    locksave = false;
}

void WordInterface::OpenFile(QString name, bool readonly, bool noEdit)
{
    if (name == "") return;


    ui->docView->setReadOnly(noEdit);
    QString suffix = QFileInfo(name).suffix();

    if (suffix == "docx" or suffix == "dotx" or suffix == "md") {
        QProcess docx;
        QString fromformat;

        if (suffix == "docx" or suffix == "dotx") fromformat = "docx";
        if (suffix == "doc") fromformat = "doc";
        else if (suffix == "md") fromformat = "markdown";

        bool return_;

        if (qEnvironmentVariableIsSet("SNAP"))
            docx.start(qEnvironmentVariable("SNAP") + "/usr/bin/pandoc -f " + fromformat + " -t html -s " + name);
        else
            docx.start("/usr/bin/pandoc", QStringList() << name), qDebug("@Spectrum: Called Pandoc Locally.");


        readonly = true;

        if (docx.waitForStarted()) {
            //docx.write(ui->docView->toCleanHtml().toUtf8());
            //docx.closeWriteChannel();
            docx.waitForFinished();
            ui->docView->setHtml(docx.readAll());
	} else {
            qDebug("Ow! You don't have pandoc.");
	}

    } else if (suffix == "odt" or suffix == "ott") {
        OOO::Converter odt;
        ui->pageview->setChecked(true);
        ui->docView->setDocument(odt.convert(name));
	OOO::StyleInformation* oooStyles = odt.getStyleInformation();
	GenerateAutoStyles(oooStyles);

        ui->docView->setUsePageMode(true);
        ui->pageview->setChecked(true);
        if (ui->ribbon->isHidden()) ui->docView->setUsePageMode(false);
    } else {
	ui->docView->setUsePageMode(true);
	ui->pageview->setChecked(true);
        QFile import(name);
        import.open(QIODevice::ReadOnly | QIODevice::Text);
        QString io = import.readAll();
        import.close();

        if (suffix == "html" or suffix == "xhtml") {
            ui->docView->setHtml(io);
        } else {
           ui->docView->setUsePageMode(false);
           ui->webview->setChecked(true);

           ui->docView->setPlainText("");
           ui->docView->setFontPointSize(11);
           ui->docView->setFontFamily("Roboto Mono");
           if (!ui->ribbon->isHidden()) HideTabs();
           ui->docView->setAcceptRichText(false);
           plaintextmode = true;
           ui->docView->setPlainText(io);
	   ui->docView->selectAll();
           QTextCursor c = ui->docView->textCursor();
           c.clearSelection();
	   c.setPosition(0);
           ui->docView->setTextCursor(c);
        }
    }

    if (!readonly) {
        document.filename = name;
        document.suffix   = suffix;
        ui->startsave->hide();
        ui->save->hide();
        ui->autosavealert_2->show();
        ui->openfilelocation->show();
    } else {
        ui->startsave->show();
        ui->save->show();
        nostart = true;
    }

    ui->docView->document()->setModified(false);
    //setWindowTitle();
    slotSetTitle();
    return;
}

void WordInterface::CreateBlankDocument(QString loc)
{
   if (sender() == ui->createsimple) loc = "SIMPLE";

    if (!nostart) {
        //ui->docView->setUsePageMode(true);
        nostart = true;
        ShowScreen("EDIT");
/*	if (loc == "SINGLESPACE") {
	    ui->docView->setPlainText("Start writing your document here.");
	} else {
            ui->docView->setPlainText("");
	    ui->docView->selectAll();
	    auto doc = ui->docView->document();
            auto blockFormat = doc->begin().blockFormat();
            blockFormat.setTopMargin(0);
	    blockFormat.setBottomMargin(0);
	    blockFormat.setLineHeight(100, QTextBlockFormat::ProportionalHeight);
            QTextCursor{doc->begin()}.setBlockFormat(blockFormat);
	    QTextCursor c = ui->docView->textCursor();
	    QTextBlockFormat bf = c.blockFormat();
            bf.setLineHeight(0, QTextBlockFormat::SingleHeight);
	    bf.setTopMargin(9);
	    bf.setBottomMargin(0);
	    c.setBlockFormat(bf);
	    c.clearSelection();
	    ui->docView->setTextCursor(c);
	}*/

        if (loc == "SIMPLE") {
            OpenFile(":/Templates/files/SimpleDocument.odt", true);
        }

        ui->docView->document()->setModified(false);
        setWindowTitle();
        ui->startsave->show();
    } else {
        WordInterface* interface = new WordInterface();
	interface->CreateBlankDocument(loc);
	interface->Begin();
    }
}

void WordInterface::EditingCommands()
{
    if (restrict) return;
    if (sender() == ui->fontSizeComboBox) {
        ui->docView->setFontPointSize(sender()->property("currentText").toReal());
    } else if (sender() == ui->fontComboBox) {
        ui->docView->setFontFamily(sender()->property("currentText").toString());
    } else if (sender() == ui->eraseFormat) {
        QTextCharFormat fmt;
        QTextCursor cursor = ui->docView->textCursor();
        cursor.setCharFormat(fmt);
        ui->docView->setTextCursor(cursor);
    }
}

void WordInterface::onTextChanged()
{
    slotSetTitle();
    onCursorUpdate();

    /*
    if (ui->docView->toPlainText().endsWith("\n")) {
        if (ui->docView->textCursor().blockFormat().headingLevel() != 0) ui->eraseFormat->click();
    }*/
}

void WordInterface::onCursorUpdate()
{
    /*if (ui->docView->currentCharFormat().underlineStyle() != QTextCharFormat::NoUnderline) underlineColorAction->setEnabled(false);
    else underlineColorAction->setEnabled(true);*/

	restrict = true;
        QString mFontSize = QString::number(ui->docView->fontPointSize());
        if (mFontSize == "0") {
                ui->fontSizeComboBox->setEditText(QString::number(10));
		ui->docView->setFontPointSize(10);
	} else {
                ui->fontSizeComboBox->setEditText(mFontSize);
        }

        QString mFontFamily = ui->docView->currentCharFormat().fontFamily();
        if (mFontFamily.isEmpty() or mFontFamily.isNull()) {
            ui->fontComboBox->setCurrentFont(QFont("Open Sans"));
	    ui->docView->setFontFamily("Open Sans");
	} else {
            ui->fontComboBox->setCurrentFont(QFont(mFontFamily));
	}

        ui->setBold->setChecked((ui->docView->fontWeight() == QFont::Bold) ? true : false);
        ui->setItalic->setChecked(ui->docView->fontItalic());
        ui->setUnderline->setChecked(ui->docView->currentCharFormat().underlineStyle() != QTextCharFormat::NoUnderline);
        ui->setStrikeout->setChecked(ui->docView->currentCharFormat().fontStrikeOut());
        ui->setSubscript->setChecked(ui->docView->currentCharFormat().verticalAlignment() == QTextCharFormat::AlignSubScript);
        ui->setSuperscript->setChecked(ui->docView->currentCharFormat().verticalAlignment() == QTextCharFormat::AlignSuperScript);

        if (ui->docView->textCursor().currentList() == nullptr) {
            ui->bulletedlist->setChecked(false);
        } else {
            ui->bulletedlist->setChecked(true);
        }

        Qt::Alignment align = ui->docView->alignment();
        if (align == Qt::AlignLeft) {
                ui->alignleft->setChecked(true);
	} else if (align == Qt::AlignRight) {
                ui->alignright->setChecked(true);
        } else if (align == Qt::AlignCenter || align == Qt::AlignHCenter) {
                ui->aligncenter->setChecked(true);
        } else if (align == Qt::AlignJustify) {
                ui->alignjustify->setChecked(true);
        }
	restrict = false;

}
