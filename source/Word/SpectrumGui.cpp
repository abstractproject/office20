/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "WordInterface.h"
#include "src/ui_BaseUserInterface.h"

#include <QtWidgets>
#include <QQuickItem>

void WordInterface::TalkToSpectrum() {
     QString what = ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("greeting")->property("text").toString();
     ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("greeting")->setProperty("text", "");

     QString output = "Ask me anything";

     if (what == "") return;

     if (what.contains("save", Qt::CaseInsensitive) && nostart) {
               if (!nostart) output = "Try creating or opening a document to do that.";
               else if (document.filename != "") output = "I'm AutoSaving this document for you.";
               else if (what.contains("as", Qt::CaseInsensitive)) {
                   if (what.contains("pdf", Qt::CaseInsensitive))
                       ui->saveAsPDF->click();
                   else if (what.contains("web", Qt::CaseInsensitive) or what.contains("html", Qt::CaseInsensitive))
                      ui->saveAsWeb->click();
                   else
                      ui->saveAs->click();
               } else
                   ui->save->click();
     } else if (what.contains("open", Qt::CaseInsensitive)) {
                if (what.contains("location", Qt::CaseInsensitive))
                   if (document.filename == "")
                       output = "Try saving this document somewhere so I can show your where it is!";
                   else
                       ui->openfilelocation->click();
               else
                   ui->openButton->click();
     } else if (what.contains("print", Qt::CaseInsensitive)) ui->print->click();
     else if (what.contains("delete", Qt::CaseInsensitive) && nostart) {
            //if (what.contains("file", Qt::CaseInsensitive) or what.contains("document", Qt::CaseInsensitive)) {
                 ui->deleteFile->click();
            //} else output = "What do you want to delete?";
    } else if (what.contains("create", Qt::CaseInsensitive) or what.contains("make", Qt::CaseInsensitive)) {
            ui->blankfromtemplate->click();
    } else output = "Sorry, I can't do that!";
    ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("greeting")->setProperty("placeholderText", output);
}

void WordInterface::SetupSpectrum()
{
    QString startsay;
    bool starter = true;

    QString uname = ui->settingsview->rootObject()->findChild<QObject*>("userName")->property("text").toString();

    if (searchlist.size() == 0) {
       //searchlist << "help me, spectrum" << "save this document" << "undo" << "redo" << "italicize this" << "make this text bold";
       searchlist << "help me, spectrum" << "save" << "open" << "create" << "save as" <<
                     "save as pdf" << "save as web page" << "save as html" << "open location" << "print" << "delete document" <<
                     "make italic" << "make bold" << "make underline" << "make strikethrough" << "undo" << "redo" <<
                     "hi spectrum!" << "unset italic" << "unset bold" << "unset underline" << "unset strikethrough" <<
                     "set italic" << "set bold" << "set underline" << "set strikethrough" << "show ribbon" << "show file tab" <<
                     "what is the time?" << "what is the date?" << "what day of the week is it?"
                     "tell time" << "tell date" << "tell day of week" << "show tabs only" << "hide ribbon" << 
                     "show sidebar" << "show help" << "show home";
       startsay = "I'm Spectrum. Need some help?";
    } else {
       starter = false;
       startsay = "Welcome back! I'm here to help.";
       if (uname != "") startsay = "Welcome back, " + uname.section(' ', 0, 0) + "! Ask me anything.";
    }

    QCompleter *askme = new QCompleter(searchlist);
    askme->setCaseSensitivity(Qt::CaseInsensitive);


    QStandardItemModel* model = new QStandardItemModel();

    // initialize the model
    int rows = searchlist.count();  // assuming this is a QStringList
    model->setRowCount(rows);
    model->setColumnCount(1);

    // load the items
    int row = 0;
    QString name;
    foreach(name, searchlist) {
        QStandardItem* item = new QStandardItem(name);
        item->setIcon(QIcon(":/basic/clock_small@1x.png"));
        model->setItem(row, 0, item);
       row++;
    }

    //askme->setModel(model);
    //askme->popup()->setModel(model); // may or may not be needed
    askme->setCompletionMode(QCompleter::InlineCompletion);
    ui->spectrumSearchInput->setCompleter(askme);

    /* Add DT */
    QStringList greetings;
    greetings << "This is Office20." << "Hi." << "Hello there." << "Hello!" << "Greetings!" << "Hey there!" << "Welcome." << "Good day." << "Let's get started.";

    int greet = QRandomGenerator::global()->bounded(greetings.size());
    greeter = greetings.at(greet);
    if (greeter == "Welcome." && !starter) {
        greeter = "Welcome back!";
        if (uname != "") {
            greeter = "Welcome back, " + uname.section(' ', 0, 0) + "!";
        }
    }

    //if (ui->optionsUserName->text() != "") ui->name->hide();

    QStringList afterask;
    afterask << "Let's try another one!" << "Have another question?" << "Need some more help?" << "I'm here to help." << "What can I do now?" << "Ask me anything." <<
                "Need something?" << "What's on your mind?" << "Anything I can do for you?" << "How can I help?";

    QDate date = QDate::currentDate();

    if (greeter == "DT") {
        if (date.dayOfWeek() == 1) greeter = "Happy Monday!";
        else if (date.dayOfWeek() == 2) greeter = "Happy Tuesday!";
        else if (date.dayOfWeek() == 3) greeter = "Happy Wednesday!";
        else if (date.dayOfWeek() == 4) greeter = "Happy Thursday!";
        else if (date.dayOfWeek() == 5) greeter = "Happy Friday!";
        else if (date.dayOfWeek() == 6) greeter = "Happy Saturday!";
        else if (date.dayOfWeek() == 7) greeter = "Happy Sunday!";
    }

    /*if (date.month() == 1 && date.day() == 1) greeter = "Happy New Year!";
    else if (date.month() == 12 && date.day() == 25) greeter = "Happy Cristmas!";*/

    ui->greeting->setPlaceholderText(greeter);
    //ui->spectrumname->setText(greeter);
    ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("greetings")->setProperty("text", greeter);

    ui->spectrumSearch->setText(startsay);

    QStringList filesearchlist;
    filesearchlist << "save" << "save as" << "save as pdf" << "save as web" << "save as html" << "open document" << "open exisiting" <<
                      "open location" << "print" << "delete document" << "delete file" << "create new document";// << "make new document";
    QCompleter *filesearch = new QCompleter(filesearchlist);
    filesearch->setCaseSensitivity(Qt::CaseInsensitive);
    filesearch->setCompletionMode(QCompleter::InlineCompletion);
    //ui->talkToSpectrum->setCompleter(filesearch);

    connect(ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("greeting"), SIGNAL(accepted()), SLOT(TalkToSpectrum()));
    connect(ui->spectrumSearchInput, &QLineEdit::returnPressed, [=]() {
		        QString what = ui->spectrumSearchInput->text();
		        ui->spectrumSearchInput->setText("");
                        //ui->spectrumSearch->clearFocus();

                        bool error = false;
                        bool set = false, unset = false, show = false, make = false, hide = false, answer = false, greeting = false, is = false, close = false;

                        unset = (what.contains("unset", Qt::CaseInsensitive)) ? true : false;
                        set = (what.contains("set", Qt::CaseInsensitive) && !unset) ? true : false;
                        show = (what.contains("show", Qt::CaseInsensitive)) ? true : false;
                        hide = (what.contains("hide", Qt::CaseInsensitive) or what.contains("close", Qt::CaseInsensitive) or
                               (show && (what.contains("dont", Qt::CaseInsensitive) or
                                         what.contains("don't", Qt::CaseInsensitive)))) ? true : false;
                        show = (hide) ? false : show;
                        make = (what.contains("make", Qt::CaseInsensitive)) ? true : false;
                        answer = (what.contains("what", Qt::CaseInsensitive) or
                                what.contains("tell", Qt::CaseInsensitive)) ? true : false;
                        //is = (what.contains("is", Qt::CaseInsensitive)) ? true : false;

                        int oo = QRandomGenerator::global()->bounded(afterask.size());
                        QString output = afterask.at(oo);
                        if (plaintextmode) output += "<p style='font-size: 9pt;'>You seem to be editing a plain text file. In plain text mode, some of my features are disabled.</p>";


                        if (what.contains("undo", Qt::CaseInsensitive)) {
                             ui->docView->undo();
                        } else if (what.contains("redo", Qt::CaseInsensitive)) {
                            ui->docView->redo();
                        /*} else if (what.contains("hey", Qt::CaseInsensitive) or 
                                 what.contains("hello", Qt::CaseInsensitive) or 
                                 (what.contains("hi", Qt::CaseInsensitive) and !hide)) {
                            ShowScreen("SPECTRUM");
                            
                            ui->spectrumname->setText("Hi! I'm Spectrum.");
                            if (ui->optionsUserName->text() != "")
                                ui->spectrumname->setText("Hi, " + ui->optionsUserName->text().section(' ', 0, 0) + "! I'm Spectrum.");
                        //else if (what.contains("whats new", Qt::CaseInsensitive)) ui->docView->undo();*/
                        } else if (what.contains("center", Qt::CaseInsensitive)) {
                            ui->aligncenter->click();
                        } else if (what.contains("justify", Qt::CaseInsensitive)) {
                            ui->alignjustify->click();
                        } else if (what.contains("left", Qt::CaseInsensitive) and what.contains("align", Qt::CaseInsensitive)) {
                            ui->alignleft->click();
                        } else if (what.contains("right", Qt::CaseInsensitive) and what.contains("align", Qt::CaseInsensitive)) {
                            ui->alignright->click();
                        } else if (what.contains("save", Qt::CaseInsensitive)) {
                           if (what.contains("as", Qt::CaseInsensitive)) {
                                if (what.contains("pdf", Qt::CaseInsensitive))
                                    ui->saveAsPDF->click();
                                else if (what.contains("web", Qt::CaseInsensitive) or what.contains("html", Qt::CaseInsensitive))
                                    ui->saveAsWeb->click();
                                else
                                    ui->saveAs->click();
                            } else {
                                if (document.filename == "") 
                                    ui->save->click();
                                else
                                    output = "I'm AutoSaving your document, so you don't need to save it yourself.";
                            }
                        } else if (what.contains("open", Qt::CaseInsensitive)) {
                            if (what.contains("location", Qt::CaseInsensitive))
                                ui->openfilelocation->click();
                            else if (what.contains("document", Qt::CaseInsensitive) or what.contains("file", Qt::CaseInsensitive) or
                                     what.contains("existing", Qt::CaseInsensitive))
                                ui->openButton->click();
                            else
                                output = "What do you want to open?";
                        } else if (what.contains("print", Qt::CaseInsensitive)) ui->print->click();
                        else if (what.contains("delete", Qt::CaseInsensitive)) {
                            if (what.contains("file", Qt::CaseInsensitive) or what.contains("document", Qt::CaseInsensitive)) {
                                ui->deleteFile->click();
                            } else output = "What do you want to delete?";
                        } else if (what.contains("create", Qt::CaseInsensitive)) ui->blankfromtemplate->click();
                        else if (what.contains("paste", Qt::CaseInsensitive)) ui->docView->paste();
                        else if (what.contains("copy", Qt::CaseInsensitive)) ui->docView->copy();
                        else if (what.contains("cut", Qt::CaseInsensitive)) ui->docView->cut();
                        else if (what.contains("italic", Qt::CaseInsensitive) or what.contains("accent", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextItalic(true);
                            else if (unset) ui->docView->setTextItalic(false);
                            //else if (is) output = QString(ui->setItalic->isChecked() ? "Yes, this text is italic." : "No, it isn't italic.");
                            else ui->docView->setTextItalic(!ui->setItalic->isChecked());
                        } else if (what.contains("bold", Qt::CaseInsensitive) or what.contains("emphasize", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextBold(true);
                            else if (unset) ui->docView->setTextBold(false);
                            //else if (is) output = QString(ui->setBold->isChecked() ? "Yes, this text is bold." : "No, it isn't bold.");
                            else ui->docView->setTextBold(!ui->setBold->isChecked());
                        } else if (what.contains("underline", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextUnderline(true);
                            else if (unset) ui->docView->setTextUnderline(false);
                            //else if (is) output = QString(ui->setUnderline->isChecked() ? "Yes, this text is underlined." : "No, it isn't underlined.");
                            else ui->docView->setTextUnderline(!ui->setUnderline->isChecked());
                        } else if (what.contains("strikeout", Qt::CaseInsensitive) or
                                   what.contains("strikethrough", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextStrikeOut(true);
                            else if (unset) ui->docView->setTextStrikeOut(false);
                            //else if (is) output = QString(ui->setStrikeout->isChecked() ? "Yes, this text is strikethrough." : "No, it isn't strikethrough.");
                            else ui->docView->setTextStrikeOut(!ui->setStrikeout->isChecked());
                        } else if (answer) { /* What is the time?, etc*/
                            if (what.contains("time", Qt::CaseInsensitive))
                                output = "The time is " + QTime::currentTime().toString("H:m:s a");
                            else if (what.contains("date", Qt::CaseInsensitive)) 
                                output = "Today is " + QDate::currentDate().toString("MMMM d yyyy");
                            else if (what.contains("day", Qt::CaseInsensitive))
                                output = "It's " + QDate::currentDate().toString("dddd!");
                            else if (what.contains("name", Qt::CaseInsensitive)) {
                                if (what.contains("your", Qt::CaseInsensitive)) output = "My name is Spectrum. You can ask me anything.";
                            }
                        } else if (show or hide) {
                            if (what.contains("ribbon", Qt::CaseInsensitive)) {
                                if (show) ShowTabs();
                                else HideRibbon();
                                if (plaintextmode) output = "Sorry, the ribbon is disabled when viewing plain text files. Try 'show tabs only' instead, or 'show file tab'.";
                            } else if (what.contains("tabs only", Qt::CaseInsensitive)) {
                               if (show) HideTabs();
                               else output = "Sorry, I can't do that.";
                            } else if (what.contains("sidebar", Qt::CaseInsensitive) or what.contains("help", Qt::CaseInsensitive)) {
                               if (show) ShowSidebar();
                               else CloseSidebar();
                            } else if (what.contains("file tab", Qt::CaseInsensitive) or what.contains("home", Qt::CaseInsensitive)) {
                                 ShowScreen("START");
                                 ui->switcher->setCurrentIndex(0);
                                 ui->Home->setChecked(true);
                            }
                        } else if (what.contains("help", Qt::CaseInsensitive)) {
                            ShowSidebar();
                            ui->helpindex->setCurrentIndex(2);
                            output = "Need some more help?";
                        } else output = "Sorry, I didn't understand that. Try rephrasing, or say 'Help'!", error = true;

                        ui->spectrumSearch->setText("<body style='font-family: Roboto Mono;'>"+output+"</body>");
                        if (!searchlist.contains(what) && !error) searchlist.prepend(what);
                        askme->setModel(new QStringListModel(searchlist, askme));
                        //qDebug() << searchlist;
		    });
}
