/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "WordInterface.h"
#include "src/ui_BaseUserInterface.h"
#include <converter.h>

#include <QTest>
#include <QQuickItem>
#include <QtWidgets>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

/*
 Initialize Word20
 */

WordInterface::WordInterface(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::WordInterface)
    , tableh(new QSpinBox)
    , tablew(new QSpinBox)
    , url(new QLineEdit)
{
    // setup user interface
    ui->setupUi(this);
    nostart = false;
    setWindowTitle();

    ui->openMSWordButton->hide();
    //QMovie *ss = new QMovie(":/CortanaAnimationJa.gif");
    //ui->spectrumanimation->setMovie(ss);
    //ss->start();
    //ui->spectrumanimation->hide();

    // hide some disabled features
    ui->startsave->hide();
    ui->setOverline->hide();
    //ui->ribboncontrol->hide();
    ui->enableautosave->hide();
    //ui->alert->hide();
    ui->autosavealert_2->hide();
    //ui->insertLink->hide();
    //ui->url->hide();
    //ui->ribbon->removeTab(2);
    ui->openfilelocation->hide();
    ui->deleteFile->hide();
    ui->insertEmoji->hide(); //XXX add emoji
    ui->saveAsWeb->hide();

    #ifdef Q_OS_DARWIN
/*    QFile File(":/style/ribbon/RibbonStyle.mac.css");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    ui->ribbon->setStyleSheet(StyleSheet);
    ui->clipboard->setAutoRaise(false);
    ui->bulletedlist->setAutoRaise(false);
    ui->setColor->setAutoRaise(false);
    ui->setBgColor->setAutoRaise(false); TODO*/
    #endif

    ribbonminsize = ui->ribbon->tabBar()->size().height();

    // Show the HOME page
    ui->screenswitch->setCurrentIndex(3);

    ReadOptions();

    // SpectrumOnly mode support
//    if (ui->ribbon->isHidden())
//        ui->docView->setUsePageMode(false);
//    else
        //ui->docView->setUsePageMode(true);

    // Display splash screen with fade effect
    if (ui->displaysplashscreen->isChecked() and !spectrumOnly) {
         ShowScreen("SPLASH");

	 Display();
         //ui->spectrumSearch->setPlaceholderText("Getting things ready...");
	 QTest::qWait(1000);
    } else if (spectrumOnly) {
         ShowScreen("START");
         Display();
         QTest::qWait(2000);
    } else {
         ShowScreen("START");
    }

    ui->readingBrowser->setUsePageMode(true);
    // TODO ui->readingBrowser->setBackgroundColor("#262626");

    ui->ribbon->setCurrentIndex(1);
    ui->Home->setChecked(true);
    ui->startHome->setChecked(true);
    CreateRibbonMenus();
    ui->viewModes->setCurrentIndex(0);
    ui->switcher->setCurrentIndex(0);

    ui->zoomIn->hide();
    ui->zoomOut->hide();

    ui->readingBrowser->setUsePageMode(true);
    ui->readingBrowser->setPageContinuous(true);
    ui->docView->installEventFilter(this);
    GenerateAutoStyles();
    BeginConnections();
    ui->fontSizeComboBox->setEditText(QString::number(10));
    SetupSpectrum();
    
    ui->label_10->hide();
}

void WordInterface::Begin()
{
    if (!ui->showstartscreen->isChecked() or document.filename != "") nostart = true;
    if (!ui->displaysplashscreen->isChecked()) {
        if (nostart) ShowScreen("EDITNOANIM");
	Display();
    } else {
       if (nostart) ShowScreen("EDITFADE");
       else {
          if (!spectrumOnly)
              ShowScreen("STARTFADE");
          else
              QMetaObject::invokeMethod(ui->talkToSpectrumWid->rootObject(), "onInitDone");

       }
    }
}

/* Destructor: delete UI */
WordInterface::~WordInterface()
{
    delete ui;
}

/* Why is this here? */
void WordInterface::Display()
{
    show();
}

void WordInterface::CreateRibbonMenus()
{
    QAction* autohideribbon = new QAction("Hide Ribbon", this);
    QAction* showtabs = new QAction("Show Tabs Only", this);
    QAction* showtabsandcommands = new QAction("Show Tabs and Commands", this);
    QAction* showsidebar = new QAction("Show Sidebar", this);
    QMenu* ribboncontrolmenu = new QMenu(this);
    ribboncontrolmenu->addAction(autohideribbon);
    ribboncontrolmenu->addAction(showtabs);
    ribboncontrolmenu->addAction(showtabsandcommands);
    ribboncontrolmenu->addAction(showsidebar);
    ui->ribboncontrol->setMenu(ribboncontrolmenu);
    connect(autohideribbon, SIGNAL(triggered()), SLOT(HideRibbon()));
    connect(showtabs, SIGNAL(triggered()), SLOT(HideTabs()));
    connect(showtabsandcommands, SIGNAL(triggered()), SLOT(ShowTabs()));
    connect(showsidebar, SIGNAL(triggered()), SLOT(ShowSidebar()));

    QMenu* listMenu = new QMenu(this);

    bulletList.append(listMenu->addAction("Disk"));
    bulletList.append(listMenu->addAction("Circle"));
    bulletList.append(listMenu->addAction("Square"));
    bulletList.append(listMenu->addAction("1. 2. 3."));
    bulletList.append(listMenu->addAction("a. b. c."));
    bulletList.append(listMenu->addAction("A. B. C."));
    bulletList.append(listMenu->addAction("i. ii. iii."));
    bulletList.append(listMenu->addAction("I. II. III."));

    for (QAction* action : bulletList) {
        connect(action, SIGNAL(triggered()), SLOT(setTextListOn()));
    }

    connect(ui->bulletedlist, SIGNAL(released()), SLOT(setTextListOn()));
    ui->bulletedlist->setMenu(listMenu);

    QMenu* insertTable = new QMenu(this);

    QWidgetAction* hact = new QWidgetAction(this);
    QWidgetAction* wact = new QWidgetAction(this);

    tableh->setValue(3);
    tablew->setValue(5);

    hact->setDefaultWidget(tableh);
    wact->setDefaultWidget(tablew);

    tableh->setStyleSheet("#tableh {padding: 5px;}");

    insertTable->addAction(hact);
    insertTable->addAction(wact);

    ui->insertTable->setMenu(insertTable);

    QMenu* insertLinkMenu = new QMenu(this);
    QWidgetAction* urlact = new QWidgetAction(this);
    url->setPlaceholderText("URL to insert");
    urlact->setDefaultWidget(url);
    insertLinkMenu->addAction(urlact);
    ui->insertLink->setMenu(insertLinkMenu);

    QMenu* lineSpacing = new QMenu(this);
   
    spacingList.append(lineSpacing->addAction("1.0"));
    spacingList.append(lineSpacing->addAction("1.15"));
    spacingList.append(lineSpacing->addAction("1.5"));
    spacingList.append(lineSpacing->addAction("2.0"));
    spacingList.append(lineSpacing->addAction("2.5"));

    for (QAction* action : spacingList) {
        connect(action, SIGNAL(triggered()), SLOT(setTextSpacing()));
    }

    ui->spacingButton->setMenu(lineSpacing);

    QMenu* clipboard = new QMenu(this);
    connect(clipboard->addAction(QIcon(":/AxIcons/clipboard-cut_small@1x.png"), "Cut"), &QAction::triggered, ui->docView, &QTextEdit::cut);
    connect(clipboard->addAction(QIcon(":/AxIcons/clipboard-copy_small@1x.png"), "Copy"), &QAction::triggered, ui->docView, &QTextEdit::copy);
    connect(clipboard->addAction(QIcon(":/AxIcons/clipboard-paste_small@1x.png"), "Paste"), &QAction::triggered, ui->docView, &QTextEdit::paste);
    connect(clipboard->addAction(QIcon(":/AxIcons/clipboard-paste-text-only_small@1x.png"), "Paste (Text Only)"), &QAction::triggered, 
		    [=]() {
		       const QClipboard *clipboard = QApplication::clipboard();
                       const QMimeData *mimeData = clipboard->mimeData();
		       this->ui->docView->insertPlainText(mimeData->text());
		    });
    ui->clipboard->setMenu(clipboard); 

    QMenu *setColorMenu = new QMenu(this);
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/default.png"), "Automatic"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.clearForeground();ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/red.png"), "Red"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#ff0000"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/orange.png"), "Orange"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#ffaa00"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/yellow.png"), "Yellow"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#ffff00"));ui->docView->setCurrentCharFormat(a);});

    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/pink.png"), "Pink"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#ff00ff"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/purple.png"), "Purple"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#aa00ff"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/blue.png"), "Blue"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#0000ff"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/green.png"), "Green"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#00ff00"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/cyan.png"), "Cyan"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#00ffff"));ui->docView->setCurrentCharFormat(a);});


    connect(setColorMenu->addAction(QIcon(":/AxIcons/colors-filled_small@1x.png"), "Select Color..."), &QAction::triggered, ui->docView, [=]() {
		    QTextCharFormat a = ui->docView->currentCharFormat();
		    QColor color = QColorDialog::getColor(a.foreground().color(), this, "Select Color");
                    a.setForeground(QBrush(color));
                    ui->docView->setCurrentCharFormat(a);
                    });
    ui->setColor->setMenu(setColorMenu);

    QMenu *setBColorMenu = new QMenu(this);

    connect(setBColorMenu->addAction(QIcon(":/Style/colorpics/yellow.png"), "Yellow"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setBackground(QBrush("#ffff00"));ui->docView->setCurrentCharFormat(a);});
    connect(setBColorMenu->addAction(QIcon(":/Style/colorpics/green.png"), "Green"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setBackground(QBrush("#00ff00"));ui->docView->setCurrentCharFormat(a);});
    connect(setBColorMenu->addAction(QIcon(":/Style/colorpics/cyan.png"), "Cyan"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setBackground(QBrush("#00ffff"));ui->docView->setCurrentCharFormat(a);});

    connect(setBColorMenu->addAction(QIcon(":/AxIcons/colors-filled_small@1x.png"), "Other..."), &QAction::triggered, ui->docView, [=]() {
                    QTextCharFormat a = ui->docView->currentCharFormat();
                    QColor color = QColorDialog::getColor(a.background().color(), this, "Select Color");
                    a.setBackground(QBrush(color));
                    ui->docView->setCurrentCharFormat(a);
                    });
    connect(setBColorMenu->addAction("Transparent"), &QAction::triggered, ui->docView, [=]() {
                    QTextCharFormat a = ui->docView->currentCharFormat();
                    a.clearBackground();
                    ui->docView->setCurrentCharFormat(a);
                    });

    ui->setBgColor->setMenu(setBColorMenu);

}


void WordInterface::ShowSidebar()
{
    if (!ui->sidebarframe->isHidden()) return;

    ui->helpindex->setCurrentIndex(0);
    ui->sidebarframe->show();
    //ui->sidebarframe->setGeometry(ui->sidebarframe->width(), 0, ui->sidebarframe->width(), ui->sidebarframe->height());
    
    // then a animation:
    QPropertyAnimation *animation = new QPropertyAnimation(ui->sidebarframe, "pos");
    animation->setDuration(100);
    animation->setEndValue(ui->sidebarframe->pos());
    animation->setStartValue(QPoint(ui->sidebarframe->pos().x()+ui->sidebarframe->width(),ui->sidebarframe->pos().y()));
    
    // to slide in call
    animation->start();
    connect(animation, &QPropertyAnimation::finished, [=](){ this->ui->sidebarframe->show(); });
}

void WordInterface::HideRibbon()
{
    ui->ribbon->hide();
    ui->statusbar->hide();
    tabshidden = false;
    ui->docView->setUsePageMode(false);
    /*ui->user->hide(), ui->ribboncontrol->hide();*/
}

void WordInterface::ShowTabs()
{
    if (plaintextmode) return;
    ui->ribbon->setMaximumSize(16777215, 16777215);
    ui->ribbon->show();
    ui->statusbar->show();
    tabshidden = false;
    if (ui->pageview->isChecked()) ui->docView->setUsePageMode(true);
    else ui->docView->setUsePageMode(false);
    /*ui->user->show(), ui->ribboncontrol->show();*/
}

void WordInterface::HideTabs()
{
    ui->ribbon->setMaximumSize(16777215, ribbonminsize+4);
    ui->ribbon->show();
    ui->statusbar->show();
    tabshidden = true;
    if (ui->pageview->isChecked()) ui->docView->setUsePageMode(true);
    else ui->docView->setUsePageMode(false);
    /*ui->user->show(), ui->ribboncontrol->show();*/
}

void WordInterface::Dictation()
{
    /*if (sender() == ui->dictation) {
        if (ui->dictation->isChecked()) speech->say(ui->docView->toPlainText());
	else speech->stop();
    } else if (sender() == speech && speech->state() == QTextToSpeech::Ready) {
        ui->dictation->setChecked(false);
    }*/
}

void WordInterface::SetView()
{
    if (sender() != ui->readingMode) ui->viewModes->setCurrentIndex(0);

    if (sender() == ui->pageview) ui->docView->setUsePageMode(true), ui->docView->setPageContinuous(false);
    else if (sender() == ui->webview) ui->docView->setUsePageMode(false);
    else if (sender() == ui->scrollview) ui->docView->setUsePageMode(true), ui->docView->setPageContinuous(true);
    else if (sender() == ui->readingMode) {
        ui->readingBrowser->setHtml(ui->docView->toHtml());
        ui->viewModes->setCurrentIndex(1);

        //if (ui->showInFullscreen->isChecked()) showFullScreen();
        //else showNormal();
	//showFullScreen();
    }

    //} else if (sender() == ui->audioMode) {
    //    ui->viewModes->setCurrentIndex(1);
    //    reader.say(ui->docView->toPlainText());
    //    //ui->texttoread->setPlainText(ui->docView->toPlainText());        
    //}
}

void WordInterface::slotSetTitle() {
    setWindowTitle("");
    if (ui->enableautosave->isChecked() && document.filename != "") {
        SaveDocument();
    }
    int wordCount = ui->docView->toPlainText().split(QRegExp("(\\s|\\n|\\r)+"), QString::SkipEmptyParts).count();
    ui->charcount->setText(QString::number(ui->docView->document()->pageCount()) + " pages     " + QString::number(wordCount) + " words");
}

void WordInterface::SidebarSwitcher()
{
    //ui->startDecorations->setStyleSheet("");
    if (sender() == ui->Home || sender() == ui->startHome) {
        ui->switcher->setCurrentIndex(0);
	//ui->startDecorations->setStyleSheet("background-color: white;");
	/*if (QTime::currentTime().hour() < 12)
            ui->greeting->setText("Good Morning");
        else if (QTime::currentTime().hour() < 18)
            ui->greeting->setText("Good Afternoon");
	else
            ui->greeting->setText("Good Evening");*/

    } else if (sender() == ui->New || sender() == ui->startNew)   ui->switcher->setCurrentIndex(1);
    else if (sender() == ui->Open || sender() == ui->startOpen) ui->switcher->setCurrentIndex(2);
    else if (sender() == ui->Options || sender() == ui->startOptions) ui->switcher->setCurrentIndex(3);
    else if (sender() == ui->Save) ui->switcher->setCurrentIndex(4);
}

void WordInterface::OpenFileLocation()
{
    if (document.filename != "")
        QDesktopServices::openUrl(QUrl::fromLocalFile(QFileInfo(document.filename).path()));
}

void WordInterface::ShowScreen(QString name)
{
    if (name == "SPLASH") ui->screenswitch->setCurrentIndex(2);
    if (name == "FILE") ui->screenswitch->slideInIdx(defaultHome);
    if (name == "EDIT") ui->screenswitch->slideInIdx(1);
    if (name == "EDITNOANIM") ui->screenswitch->setCurrentIndex(1);
    if (name == "EDITFADE") ui->screenswitch->fadeInIdx(1);
    if (name == "EDIT" && ui->screenswitch->currentIndex() == 0) ui->ribbon->setCurrentIndex(1);

    if (name == "START") {
        ui->screenswitch->slideInIdx(defaultHome);
        /* Maybe do something else... */
    } else if (name == "STARTNOANIM") {
        ui->screenswitch->setCurrentIndex(defaultHome);
    } else if (name == "STARTFADE") {
        ui->screenswitch->fadeInIdx(defaultHome);
    }

    if (name == "SPECTRUM") {
        ui->screenswitch->slideInIdx(3);
        /* Maybe do something else... */
    } else if (name == "SPECTRUMNOANIM") {
        ui->screenswitch->setCurrentIndex(3);
    } else if (name == "SPECTRUMFADE") {
        ui->screenswitch->fadeInIdx(3);
    }

    if (nostart) {
        ui->startSidebar->close();
        ui->sidebars->setCurrentIndex(1);
        ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("backtoedit")->setProperty("visible", true);
    } else {
        ui->sidebars->setCurrentIndex(0);
    }
}

void WordInterface::SwitchScreens()
{
    if ((sender() == ui->ribbon && ui->ribbon->currentIndex() == 0)) {
        ShowScreen("START");
	ui->switcher->setCurrentIndex(0);
	ui->Home->setChecked(true);
	//ui->startDecorations->setStyleSheet("#startDecorations { background-color: white; }");
    } else if (sender() == ui->goback or sender() == ui->talkToSpectrumWid->rootObject()->findChild<QObject*>("backtoedit")) {
        ShowScreen("EDIT");
	ui->ribbon->setCurrentIndex(1);
    }
}

void WordInterface::CloseSidebar()
{
    //ui->sidebarframe->setGeometry(ui->sidebarframe->width(), 0, ui->sidebarframe->width(), ui->sidebarframe->height());
 
    // then a animation:
    QPropertyAnimation *animation = new QPropertyAnimation(ui->sidebarframe, "pos");
    animation->setDuration(100);
    animation->setStartValue(ui->sidebarframe->pos());
    animation->setEndValue(QPoint(ui->sidebarframe->pos().x()+ui->sidebarframe->width(),ui->sidebarframe->pos().y()));
 
    // to slide in call
    animation->start();
    connect(animation, &QPropertyAnimation::finished, [=](){ this->ui->sidebarframe->hide(); });
}

void WordInterface::HelpIndex()
{
    ShowSidebar();
    if (sender() == ui->helphome or sender() == ui->ribbonhelp) {
        ui->helpindex->slideInIdx(0);
    } else if (sender() == ui->whatsnew) {
        ui->helpindex->slideInIdx(1);
    } else if (sender() == ui->spectrumintro) {
        ui->helpindex->slideInIdx(2);
    } else if (sender() == ui->office20license) {
        ui->helpindex->slideInIdx(3);
    } else if (sender() == ui->aboutword) {
        ui->helpindex->slideInIdx(4);
    }
}
