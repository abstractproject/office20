/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef WORDINTERFACE_H
#define WORDINTERFACE_H

#include <QMainWindow>
#include <QSpinBox>

#include <QTextCharFormat>
#include <QTextBlockFormat>


#include <converter.h>
#include <Speak/SpectrumSpeak.h>

QT_BEGIN_NAMESPACE
namespace Ui { class WordInterface; }
QT_END_NAMESPACE

class WordInterface : public QMainWindow
{
    Q_OBJECT

public:
    WordInterface(QWidget *parent = nullptr);
    ~WordInterface();

    void OpenFile(QString name, bool readonly = false, bool noEdit = false);
    void SaveDocument();
    void Display();
    void Begin();

private:
   void createWidgets();

public slots:
    void CreateBlankDocument(QString loc = "");

private:
    void RemoveList();
    void CustomizeQuickAccessToolbar();
    void CreateRibbonMenus();

    void GenerateAutoStyles(OOO::StyleInformation* mStyleInformation = nullptr);
    void UpdateStyles();
    void SaveStyles(bool HTML_NOT_ODF = false);

    void BeginConnections();
    void ReadOptions();
    void WriteOptions();

    void ShowScreen(QString name);
    void SetupSpectrum();

private slots:
    void ShowSidebar();
    void HelpIndex();

    void TalkToSpectrum();
    void ConnectStyleSlots();

    void SetView();
    void SwitchScreens();
    void EditingCommands();
    // TODO void OpenFileLocation();

    void IoCommands();
    void OpenFileLocation();

    void Dictation();

    void setTextListOn();
    void setTextSpacing();

    void CloseSidebar();
    void SidebarSwitcher();
    void ShowTabs();
    void HideTabs();
    void HideRibbon();
    void slotSetTitle();

    void onCursorUpdate();
    void onTextChanged();

    void checkReturnKey(QKeyEvent *keyEvent);
protected:
    //void SetWindowTitle();
    void setWindowTitle(QString name = "");
    void closeEvent(QCloseEvent* event);
    bool eventFilter(QObject *object, QEvent *event);

private:
    Ui::WordInterface *ui;

    QStringList searchlist;

    bool tabshidden;
    qreal ribbonminsize;

    bool nostart;
    QString defaultname = "Document1";
    struct DocumentData {
        QString filename;
	QString author;
	QString title;
	QString suffix;
    } document;

    bool spectrumOnly;

    QVector<QAction*> bulletList;
    QVector<QAction*> spacingList;
    QVector<QAction*> underlineList;

    bool locksave = false;
    bool restrict = false;
    bool plaintextmode = false;

    int defaultHome = 0;

    QSpinBox *tableh, *tablew;

    QString greeter;

    bool wasMaximized;
    bool styleon = false;
    QStringList stylenames;
    QMap<QString, QTextCharFormat> styleCharFormats;
    QMap<QString, QTextBlockFormat> styleBlockFormats;
    QLineEdit *url;

    bool isSidebarMinimized = true;

    QMenu* stylemenus;
    SpectrumSpeak reader;
};

#endif // WORDINTERFACE_H
