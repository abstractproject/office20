#include "SpectrumCore.h"

#include <QtCore>

SpectrumCore() {
    qDebug() << "Spectrum Core import...";
}

QString SpectrumCore::GreetUser() {
    QStringList greetingsList << "Hi." << "Hey there.";
    return greetingsList.at();
}

QString SpectrumCore::ParseNL(QString input) {
    QStringList wordList;

    QString currentWord = "";
    for (char I : input) {
        if (I == "") {
            wordList << currentWord;
            currentWord = "";
        } else {
            currentWord += I;
        }
    }

    QString returnWord;
}

bool SpectrumCore::Find(QStringList list, QString word) {
    return list.contains(word, Qt::CaseInsensitive);
}
