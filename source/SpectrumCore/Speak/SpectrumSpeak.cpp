#include "SpectrumSpeak.h"

#include <QProcess>
#include <QDebug>

SpectrumSpeak::SpectrumSpeak() {
    #ifdef Q_OS_LINUX
    os_backend = "flite";
    voice = "awb";
    #else
    qDebug("@O20: No speech backend found.");
    #endif
}

void SpectrumSpeak::stop() {
    backend.kill();
}

void SpectrumSpeak::say(QString what) {
    QStringList args = QStringList() << "-voice" << voice;
    backend.start(os_backend, args);
    backend.waitForStarted();
    backend.write(what.toUtf8());
    backend.closeWriteChannel();
}
