#ifndef SPECTRUMCORE_HEADER
#define SPECTRUMCORE_HEADER

#include <QString>

class SpectrumCore {
public:

    SpectrumCore();
    QString GreetUser();
    bool ParseNL(QString input = "");
}

#endif
