/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "PageEditorWidget.h"

#include <cstdlib>

#include <QtWidgets>

qreal mmToInches(qreal mm) {
    return mm * 0.039370147;
}

qreal mmToPx(qreal mm, bool isx) {
    return mmToInches(mm) * (isx ? qApp->desktop()->logicalDpiX() : qApp->desktop()->logicalDpiY());
}

qreal inToPx(qreal inch, bool isx) {
    return inch * (isx ? qApp->desktop()->logicalDpiX() : qApp->desktop()->logicalDpiY());
}

PageEditorWidget::PageEditorWidget(QWidget *parent) :
    KRichTextEdit(parent),
    m_document(0),
    m_usePageMode(false),
    m_addBottomSpace(true),
    m_showPageNumbers(false),
    m_backgroundColor("#f0f0f0"),
    m_borderColor("#c6c6c6"),
    m_pageNumbersAlignment(Qt::AlignTop | Qt::AlignRight)
{
    //setStyleSheet("QTextEdit { font-family: Open Sans; font-size: 12pt; }");

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    verticalScrollBar()->setMaximum(1);

    aboutDocumentChanged();
    document()->setPageSize(QPageSize(QPageSize::Letter).size(QPageSize::Inch));
    connect(this, SIGNAL(textChanged()), this, SLOT(aboutDocumentChanged()));

    m_continuousPage = false;
    m_pageColor = QBrush("#fff").color();
    setPageMarginsInch(.5, .5, .5, .5);
    m_pageWidth = inToPx(8.5, true);
    m_pageHeight = inToPx(11, false);

    connect(verticalScrollBar(), SIGNAL(rangeChanged(int,int)), this, SLOT(aboutVerticalScrollRangeChanged(int,int)));
}

/*
void PageEditorWidget::setPageFormat(QPageSize::PageSizeId _pageFormat)
{
    m_pageMetrics.update(_pageFormat);

    repaint();
}*/

void PageEditorWidget::setBackgroundColor(const QString& _color) {
    m_backgroundColor = _color;
    if (m_usePageMode)
        setStyleSheet("QTextEdit {color: #000000; background-color:" + m_backgroundColor + "; }");
    else
        setStyleSheet("QTextEdit { background-color:" + m_backgroundColor +"; color: #000000;}");
    repaint();
}

void PageEditorWidget::setPageBorderColor(const QString& _color) {
    m_borderColor = _color;
    repaint();
}

int PageEditorWidget::currentPageNumber()
{
    return 1;
}

void PageEditorWidget::setPageColor(const QColor& _color)
{
    m_pageColor = _color;
    repaint();
}

void PageEditorWidget::setPageMargins(const QMargins& _margins)
{
    m_margins = _margins;
    repaint();
}

void PageEditorWidget::setPageMarginsInch(qreal left, qreal top, qreal right, qreal bottom)
{
    m_margins = QMargins(inToPx(left, true), inToPx(top, false), inToPx(right, true), inToPx(bottom, false));
    setPageMargins(m_margins);
}

void PageEditorWidget::setPageSize(const QSizeF& _size)
{
    m_pageWidth = _size.width();
    m_pageHeight = _size.height();
    document()->setPageSize(QSizeF(m_pageWidth, m_pageHeight));
    repaint();
}

void PageEditorWidget::setPageSizeInch(qreal width, qreal height)
{
    m_pageWidth = inToPx(width, true);
    setPageSize(QSizeF(inToPx(width, true), inToPx(height, false)));
    repaint();
}

bool PageEditorWidget::usePageMode() const
{
    return m_usePageMode;
}

void PageEditorWidget::setUsePageMode(bool _use)
{
    if (_use) {
        setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        setStyleSheet("QTextEdit {color: #000000; background-color:" + m_backgroundColor + "; }");
    } else {
        setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        setStyleSheet("QTextEdit { background-color:" + m_backgroundColor +"; color: #000000;}");
    }


    if (m_usePageMode != _use) {
        m_usePageMode = _use;

        repaint();
    }
                repaint();
}

void PageEditorWidget::setAddSpaceToBottom(bool _addSpace)
{
    if (m_addBottomSpace != _addSpace) {
        m_addBottomSpace = _addSpace;

        repaint();
    }
}

void PageEditorWidget::setShowPageNumbers(bool _show)
{
    if (m_showPageNumbers != _show) {
        m_showPageNumbers = _show;

        repaint();
    }
}

void PageEditorWidget::setPageNumbersAlignment(Qt::Alignment _align)
{
    if (m_pageNumbersAlignment != _align) {
        m_pageNumbersAlignment = _align;

        repaint();
    }
}

void PageEditorWidget::paintEvent(QPaintEvent* _event)
{
    updateVerticalScrollRange();

    paintPagesView();

    paintPageNumbers();

    QTextEdit::paintEvent(_event);
}

void PageEditorWidget::resizeEvent(QResizeEvent* _event)
{
    updateViewportMargins();

    updateVerticalScrollRange();

    QTextEdit::resizeEvent(_event);
}

void PageEditorWidget::updateViewportMargins()
{
    QMargins viewportMargins;

    if (m_usePageMode) {

        int pageWidth = m_pageWidth; // m_pageMetrics.pxPageSize().width();
        int pageHeight = m_pageHeight; // m_pageMetrics.pxPageSize().height();

        const int DEFAULT_TOP_MARGIN = 20;
        const int DEFAULT_BOTTOM_MARGIN = 20;
        {
            int leftMargin = 0;
            int rightMargin = 0;

            if (width() > pageWidth) {
                const int BORDERS_WIDTH = 4;
                const int VERTICAL_SCROLLBAR_WIDTH =
                        verticalScrollBar()->isVisible() ? verticalScrollBar()->width() : 0;
                leftMargin = rightMargin =
                        (width() - pageWidth - VERTICAL_SCROLLBAR_WIDTH - BORDERS_WIDTH) / 2;
            }

            int topMargin = DEFAULT_TOP_MARGIN;

            int bottomMargin = DEFAULT_BOTTOM_MARGIN;
            int documentHeight = pageHeight * document()->pageCount();
            if ((height() - documentHeight) > (DEFAULT_TOP_MARGIN + DEFAULT_BOTTOM_MARGIN)) {
                const int BORDERS_HEIGHT = 2;
                const int HORIZONTAL_SCROLLBAR_HEIGHT =
                        horizontalScrollBar()->isVisible() ? horizontalScrollBar()->height() : 0;
                bottomMargin =
                    height() - documentHeight - HORIZONTAL_SCROLLBAR_HEIGHT - DEFAULT_TOP_MARGIN - BORDERS_HEIGHT;
            }

            viewportMargins = QMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        }
    }

    setViewportMargins(viewportMargins);

    aboutUpdateDocumentGeometry();
}

void PageEditorWidget::updateVerticalScrollRange()
{
    if (m_usePageMode) {

        const int pageHeight = m_pageHeight; // m_pageMetrics.pxPageSize().height();
        const int documentHeight = pageHeight * document()->pageCount();
        const int maximumValue = documentHeight - viewport()->height();
        if (verticalScrollBar()->maximum() != maximumValue) {
            verticalScrollBar()->setMaximum(maximumValue);
        }
    }
    else {
        const int SCROLL_DELTA = 800;
        int maximumValue =
                document()->size().height() - viewport()->size().height()
                + (m_addBottomSpace ? SCROLL_DELTA : 0);
        if (verticalScrollBar()->maximum() != maximumValue) {
            verticalScrollBar()->setMaximum(maximumValue);
        }
    }
}

void PageEditorWidget::paintPagesView()
{
    if (m_usePageMode) {

        qreal pageWidth = m_pageWidth; // m_pageMetrics.pxPageSize().width();
        qreal pageHeight = m_pageHeight; // m_pageMetrics.pxPageSize().height();

        QPainter p(viewport());

        QPen spacePen(QBrush(m_backgroundColor, Qt::SolidPattern)/*palette().window()*/, 9);
        QPen borderPen(QBrush(m_borderColor, Qt::SolidPattern), 1);

        if (m_continuousPage) spacePen = QPen(QBrush("#f0f0f0"), 0);

        qreal curHeight = pageHeight - (verticalScrollBar()->value() % (int)pageHeight);
        const int x = pageWidth + (width() % 2 == 0 ? 2 : 1);
        const int horizontalDelta = horizontalScrollBar()->value();

        // paint the inside of the page to be white
                                qreal tomfool = curHeight;

        while(tomfool <= pageHeight) {
            p.setPen(borderPen);
            p.fillRect(0, tomfool-8, x, tomfool - pageHeight, m_pageColor);
            p.fillRect(0, tomfool - pageHeight, x, tomfool, m_pageColor);
            p.fillRect(0, curHeight-8, x, curHeight, m_pageColor);
            tomfool += pageHeight;
        }

        if (tomfool >= height()) {
            p.setPen(borderPen);
            p.fillRect(0 - horizontalDelta, tomfool-pageHeight, x - horizontalDelta, height(), m_pageColor);
        }

        if (curHeight - pageHeight >= 0) {
            p.setPen(borderPen);
            p.drawLine(0, curHeight - pageHeight, x, curHeight - pageHeight);
        }

        while (curHeight <= height()) {
            if (!m_continuousPage) {
                p.setPen(spacePen);
                p.drawLine(0, curHeight-4, width(), curHeight-4);
                p.setPen(borderPen);
                p.drawLine(0, curHeight-8, x, curHeight-8);
                p.drawLine(0, curHeight, x, curHeight);
                p.drawLine(0 - horizontalDelta, curHeight - pageHeight, 0 - horizontalDelta, curHeight - 8);
                p.drawLine(x - horizontalDelta, curHeight - pageHeight, x - horizontalDelta, curHeight - 8);
            } else {
                p.setPen(borderPen);
                p.drawLine(0, curHeight, x, curHeight);
                p.drawLine(0 - horizontalDelta, curHeight - pageHeight, 0 - horizontalDelta, curHeight/* - 8*/);
                p.drawLine(x - horizontalDelta, curHeight - pageHeight, x - horizontalDelta, curHeight/* - 8*/);
            }

            curHeight += pageHeight;
        }

        if (curHeight >= height()) {
            p.setPen(borderPen);
            p.drawLine(0 - horizontalDelta, curHeight-pageHeight, 0 - horizontalDelta, height());
            p.drawLine(x - horizontalDelta, curHeight-pageHeight, x - horizontalDelta, height());
        }
    }
}

void PageEditorWidget::paintPageNumbers()
{
    if (m_usePageMode && /*!m_pageMetrics.pxPageMargins().isNull() &&*/ m_showPageNumbers) {

        QSizeF pageSize(m_pageWidth, m_pageHeight);
        QMargins pageMargins = m_margins;

        QPainter p(viewport());
        p.setFont(document()->defaultFont());
        p.setPen(QPen(palette().text(), 1));

        qreal curHeight = pageSize.height() - (verticalScrollBar()->value() % (int)pageSize.height());

        qreal leftMarginPosition = pageMargins.left() - horizontalScrollBar()->value();
        qreal marginWidth = pageSize.width() - pageMargins.left() - pageMargins.right();

        int pageNumber = verticalScrollBar()->value() / pageSize.height() + 1;

        if (curHeight - pageMargins.top() >= 0) {
            QRectF topMarginRect(leftMarginPosition, curHeight - pageSize.height(), marginWidth, pageMargins.top());
            paintPageNumber(&p, topMarginRect, true, pageNumber);
        }

        while (curHeight < height()) {
            QRect bottomMarginRect(leftMarginPosition, curHeight - pageMargins.bottom(), marginWidth, pageMargins.bottom());
            paintPageNumber(&p, bottomMarginRect, false, pageNumber);

            ++pageNumber;

            QRect topMarginRect(leftMarginPosition, curHeight, marginWidth, pageMargins.top());
            paintPageNumber(&p, topMarginRect, true, pageNumber);

            curHeight += pageSize.height();
        }
    }
}

void PageEditorWidget::paintPageNumber(QPainter* _painter, const QRectF& _rect, bool _isHeader, int _number)
{
    if (_isHeader) {
        if (m_pageNumbersAlignment.testFlag(Qt::AlignTop)) {
            _painter->drawText(_rect, Qt::AlignVCenter | (m_pageNumbersAlignment ^ Qt::AlignTop),
                QString::number(_number));
        }
    }
    else {
        if (m_pageNumbersAlignment.testFlag(Qt::AlignBottom)) {
            _painter->drawText(_rect, Qt::AlignVCenter | (m_pageNumbersAlignment ^ Qt::AlignBottom),
                QString::number(_number));
        }
    }
}

void PageEditorWidget::aboutVerticalScrollRangeChanged(int _minimum, int _maximum)
{
    Q_UNUSED(_minimum);

    updateViewportMargins();


    int scrollValue = verticalScrollBar()->value();

    if (scrollValue > _maximum) {
        updateVerticalScrollRange();
    }
}

void PageEditorWidget::aboutDocumentChanged()
{
    if (m_document != document()) {
        m_document = document();

        connect(document()->documentLayout(), SIGNAL(update()), this, SLOT(aboutUpdateDocumentGeometry()));
    }
}

void PageEditorWidget::aboutUpdateDocumentGeometry()
{
    QSizeF documentSize(width() - verticalScrollBar()->width(), -1);
    if (m_usePageMode) {
        int pageWidth = m_pageWidth; // m_pageMetrics.pxPageSize().width();
        int pageHeight = m_pageHeight; // m_pageMetrics.pxPageSize().height();
        documentSize = QSizeF(pageWidth, pageHeight);
    }

    if (document()->pageSize() != documentSize) {
        document()->setPageSize(documentSize);
    }

    if (document()->documentMargin() != 0) {
        document()->setDocumentMargin(0);
    }
    QMargins rootFrameMargins = m_margins; // m_pageMetrics.pxPageMargins();
    QTextFrameFormat rootFrameFormat = document()->rootFrame()->frameFormat();
    if (rootFrameFormat.leftMargin() != rootFrameMargins.left()
        || rootFrameFormat.topMargin() != rootFrameMargins.top()
        || rootFrameFormat.rightMargin() != rootFrameMargins.right()
        || rootFrameFormat.bottomMargin() != rootFrameMargins.bottom()) {
        rootFrameFormat.setLeftMargin(rootFrameMargins.left());
        rootFrameFormat.setTopMargin(rootFrameMargins.top());
        rootFrameFormat.setRightMargin(rootFrameMargins.right());
        rootFrameFormat.setBottomMargin(rootFrameMargins.bottom());
        document()->rootFrame()->setFrameFormat(rootFrameFormat);
    }
}
